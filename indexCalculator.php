<html>
<head>
    <link href="master.css" rel="stylesheet" type="text/css" />
    <title>Calculator</title>

</head>

<body>

<div class="wrapperCenter">
    <div class="calcOutline">
        <div class="wrapperCenter">
            <form method="get" name="calculator">
                <table class="calculator">
                    <tr class="calcView" name="calcView">
                    <td colspan=4 id="result">
                    
                    </td>
                    </tr>
                    <tr>
                        <td colspan=4><input type="reset" class="button" name="clear" id="clear" value="Clear" /></td>
                    </tr>
                    <tr>
                        <td colspan=4><input type="number" name="numberOne" id = "numberOne" class="tField" /></td>    
                    </tr>
                    <tr class="calcType">
                        <td>
                            <label>+ <br>(Plus)</label><br> <input type="radio" name="calcType" value="add" class="calcType" checked/>
                        </td>
                        <td>
                            <label>- <br>(Minus)</label><br><input type="radio" name="calcType" value="subtract" class="calcType" />
                        </td>
                        <td>
                            <label>* <br>(Times) </label><br><input type="radio" name="calcType" value = "multiply" class="calcType" />
                        </td>
                        <td>
                            <label>/ <br>(Divide) </label><br><input type="radio" name="calcType" value = "divide" class="calcType" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan=4><input type="number" name="numberTwo" id = "numberTwo" class="tField" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<script>
var calculate = function(){
	var numOne = parseFloat(document.getElementById("numberOne").value);
	var numTwo = parseFloat(document.getElementById("numberTwo").value);
	if(isNaN(numOne)){
		numOne = 0;
	}
	if(isNaN(numTwo)){
		numTwo = 0;
	}
	var operations = document.getElementsByName("calcType");
	for (var i = 0; i < operations.length; i++) {
		if(operations[i].checked){
			var op = operations[i].value;
			break;	
		}
	}
	var result = "";
	if(op !== null && op !== undefined){
		if(op == 'add'){
			result = numOne + numTwo;
		}else if(op == 'subtract'){
			result = numOne - numTwo;	
		}else if(op == 'multiply'){
			result = numOne * numTwo;	
		}else if(op == 'divide'){
			if(numTwo == 0){
				result = 'Error: Division by 0';
			}else{
				result = numOne / numTwo;	
			}
		}		
	}
	document.getElementById("result").innerHTML = result;
}
var clear = function(){
	document.getElementById("result").innerHTML = "";	
}
document.getElementById("numberOne").addEventListener('keyup', calculate, false);
document.getElementById("numberTwo").addEventListener('keyup', calculate, false);
var operations = document.getElementsByName("calcType");
for (var i = 0; i < operations.length; i++) {
	operations[i].addEventListener('change',calculate,false);
}
document.getElementById("clear").addEventListener('click', clear, false);

</script>

</body>
</html>